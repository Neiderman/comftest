@extends('templates.base')

@section('titulo')
Actividades de usuario
@endsection


@section('contenido')
<div class="jumbotron">
    <div class="container">
        <h1>Bienvenido</h1>
        <p>Modulo para la gestión de cuidadores de estudiantes de preescolar.</p>

        <div class="form-group">
            <label for="buscador_ident">Buscador de cuidadores</label>
            <div class="input-group">
                <input type="text" class="form-control" maxlength="30" name="buscador_ident" id="buscador_ident" placeholder="Buscar cuidadores por cedula o por registro civil de nacimiento...">
                <span class="input-group-btn">
                    <button class="btn btn-success btn_filtrar" type="button">Buscar</button>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <table id="tabla_Est" class="table table-bordered">
          <thead>
              <tr>
                  <th scope="col">Identificación</th>
                  <th scope="col">Nombre completo</th>
                  <th scope="col">Tipo de usuario</th>
                  <th>Acciones</th>
              </tr>
          </thead>
          <tbody id="registros_encontrados">
          </tbody>
    </table>
  </div>
@endsection

@section('scripts')
    <script>
        var btn_accion_activa = false;
        $(document).ready( function () {

            $('.table').DataTable();

            $('.btn_filtrar').on('click', function () {
                if (btn_accion_activa) {
                    return false;
                }

                var buscador_inp = $('#buscador_ident');
                    buscador_inp.parents('div.form-group').removeClass('has-error');
                if (buscador_inp.val() == "") {
                    buscador_inp.parents('div.form-group').addClass('has-error');
                    return false;
                }

                $('.table').DataTable().destroy();
                $('#registros_encontrados').html("");

                $.ajax({
                    type: "get",
                    url: "{!! route('listadoEstudiantes') !!}",
                    data: {
                        filtro: buscador_inp.val()
                    },
                    dataType: "json",
                    success: function (response) {
                        var resultados_consulta = "";
                        $.each(response, function (index, elemento) {
                            resultados_consulta += "<tr>";
                            resultados_consulta += "  <td>"+elemento.documento+"</td>";
                            resultados_consulta += "  <td>"+elemento.nombre+"</td>";
                            resultados_consulta += "  <td>"+elemento.tipo+"</td>";
                            resultados_consulta += "  <td>";
                                <a href="{!! route("editar_estudiante", $estudiante->id) !!}" class="btn btn-info" title="Editar"><i class="fas fa-pencil-alt"></i></a>
                                <a href="{!! route("eliminar_estudiante", $estudiante->id) !!}" class="btn btn-danger" title="Eliminar"><i class="fas fa-times"></i></a>
                            resultados_consulta += "  </td>";
                            resultados_consulta += "</tr>";
                        });

                        $('#registros_encontrados').html(resultados_consulta);
                        $('.table').DataTable();
                    }
                });



            });
        });
  </script>
@endsection
