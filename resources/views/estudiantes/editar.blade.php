@extends('templates.base')

@section('titulo')
Actividades de usuario
@endsection


@section('contenido')
<div class="jumbotron">
    <div class="container">
        <a href="{!! route('inicio') !!}"><small>Volver al inicio</small></a>
        <h1>Editar estudiante</h1>
    </div>
</div>

<div class="container">
    <form action="{!! route('editar_estudiante_submt',$estudiante->id) !!}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="documento">Documento <span title="Requerido">*</span></label>
                        <input type="text" value="{{ old('documento') ?: $estudiante->documento }}" class="form-control @error('documento') is-invalid @enderror" id="documento" name="documento" maxlength="30" placeholder="Documento" required>
                        @error('documento')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="fecha_nacimiento">Fecha de nacimiento<span title="Requerido">*</span></label>
                        <input type="date" value="{{ old('fecha_nacimiento') ?: $estudiante->fecha_nacimiento }}" class="form-control @error('fecha_nacimiento') is-invalid @enderror" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Fecha de nacimiento" required>
                        @error('fecha_nacimiento')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="primer_nombre">Primer nombre <span title="Requerido">*</span></label>
                        <input type="text" value="{{ old('primer_nombre') ?: $estudiante->primer_nombre }}" class="form-control @error('primer_nombre') is-invalid @enderror" id="primer_nombre" name="primer_nombre" maxlength="30" placeholder="Primer nombre" required>
                        @error('primer_nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="segundo_nombre">Segundo nombre</label>
                        <input type="text" value="{{ old('segundo_nombre') ?: $estudiante->segundo_nombre }}" class="form-control @error('segundo_nombre') is-invalid @enderror" id="segundo_nombre" name="segundo_nombre" maxlength="30" placeholder="Segundo nombre">
                        @error('segundo_nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="primer_apellido">Primer apellido <span title="Requerido">*</span></label>
                        <input type="text" value="{{ old('primer_apellido') ?: $estudiante->primer_apellido }}" class="form-control @error('primer_apellido') is-invalid @enderror" id="primer_apellido" name="primer_apellido" maxlength="30" placeholder="Primer apellido" required>
                        @error('primer_apellido')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="segundo_apellido">Segundo apellido</label>
                        <input type="text" value="{{ old('segundo_apellido') ?: $estudiante->segundo_apellido }}" class="form-control @error('segundo_apellido') is-invalid @enderror" id="segundo_apellido" name="segundo_apellido" maxlength="30" placeholder="Segundo apellido">
                        @error('segundo_apellido')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="direccion">Dirección <span title="Requerido">*</span></label>
                        <input type="text" value="{{ old('direccion') ?: $estudiante->direccion }}" class="form-control @error('direccion') is-invalid @enderror" id="direccion" name="direccion"  maxlength="50" placeholder="Dirección" required>
                        @error('direccion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

            </div>
        </div>
        <a href="{!! route('inicio') !!}" class="btn btn-danger">Cancelar</a>
        <button type="submit" class="btn btn-success">Actualizar</button>
    </form>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $('.table').DataTable();
        });
  </script>
@endsection
