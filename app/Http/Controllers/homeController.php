<?php

namespace App\Http\Controllers;

use App\Estudiantes;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
Use Alert;
use App\Responsables;

class homeController extends Controller
{

    public function index()
    {
        return view('welcome');
    }

    public function listadoEstudiantes(Request $request)
    {
        $filtro = $request->filtro;
        $estudiantes = Estudiantes::where('documento',$filtro)->get();
        $responsables = Responsables::where('documento',$filtro)->get();

        $respuesta = [];
        foreach ($estudiantes as $estudiante) {
            $estudiante_ar = [];
            $estudiante_ar['id'] = $estudiante->id;
            $estudiante_ar['documento'] = $estudiante->documento;
            $estudiante_ar['nombre'] = $estudiante->primer_nombre.($estudiante->segundo_nombre ? ' '.$estudiante->segundo_nombre : '').($estudiante->primer_apellido ? ' '.$estudiante->primer_apellido : '').($estudiante->segundo_apellido ? ' '.$estudiante->segundo_apellido : '');
            $estudiante_ar['tipo'] = 'menor';

            $respuesta[] = $estudiante_ar;
        }

        foreach ($responsables as $responsable) {
            $responsable_ar = [];
            $responsable_ar['id'] = $responsable->id;
            $responsable_ar['documento'] = $responsable->documento;
            $responsable_ar['nombre'] = $responsable->nombre;
            $responsable_ar['tipo'] = 'responsable';

            $respuesta[] = $responsable_ar;
        }

        return json_encode($respuesta);
    }

    public function crear_estudiante()
    {
        return view('estudiantes.nuevo');
    }

    public function crear_estudiante_submt(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'documento' => 'required|max:30|',
            'primer_nombre' => 'required|max:30',
            'segundo_nombre' => 'max:30',
            'primer_apellido' => 'required|max:30',
            'segundo_apellido' => 'max:30',
            'fecha_nacimiento' => 'required|date|before:tomorrow',
            'direccion' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $estudiante = new Estudiantes();
        $estudiante->documento = $request->documento;
        $estudiante->primer_nombre = $request->primer_nombre;
        $estudiante->segundo_nombre = $request->segundo_nombre;
        $estudiante->primer_apellido = $request->primer_apellido;
        $estudiante->segundo_apellido = $request->segundo_apellido;
        $estudiante->fecha_nacimiento = $request->fecha_nacimiento;
        $estudiante->direccion = $request->direccion;

        $estudiante->save();

        Alert::success('Correcto', 'El estudiante fue almacenado con exito');
        return redirect(route('inicio'));
    }

    public function editar_estudiante(Request $request, $id)
    {
        $estudiante = Estudiantes::with('alergias','responsables')->find($id);
        if (!$estudiante) {
            Alert::error('Error', 'El estudiante no se encuentra registrado en la plataforma');
            return redirect(route('inicio'));
        }

        return view('estudiantes.editar',compact('estudiante'));
    }

    public function editar_estudiante_submt(Request $request, $id)
    {
        $estudiante = Estudiantes::find($id);
        if (!$estudiante) {
            Alert::error('Error', 'El estudiante no se encuentra registrado en la plataforma');
            return redirect(route('inicio'));
        }

        $validator = Validator::make($request->all(), [
            'documento' => 'required|max:30|',
            'primer_nombre' => 'required|max:30',
            'segundo_nombre' => 'max:30',
            'primer_apellido' => 'required|max:30',
            'segundo_apellido' => 'max:30',
            'fecha_nacimiento' => 'required|date|before:tomorrow',
            'direccion' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $estudiante->documento = $request->documento;
        $estudiante->primer_nombre = $request->primer_nombre;
        $estudiante->segundo_nombre = $request->segundo_nombre;
        $estudiante->primer_apellido = $request->primer_apellido;
        $estudiante->segundo_apellido = $request->segundo_apellido;
        $estudiante->fecha_nacimiento = $request->fecha_nacimiento;
        $estudiante->direccion = $request->direccion;

        $estudiante->save();

        Alert::success('Correcto', 'El estudiante fue actualizado con exito');
        return redirect(route('inicio'));
    }

    public function eliminar_estudiante(Request $request, $id)
    {
        return Estudiantes::with('alergias','responsables')->get();
    }
}
