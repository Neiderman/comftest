<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudiantesResponsables extends Model
{
    protected $table = "responsables_estudiantes";
    public $timestamps = false;

    public function responsables()
    {
        return $this->hasMany(Responsables::class, 'id', 'responsable_id');
    }
}
