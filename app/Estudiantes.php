<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiantes extends Model
{
    protected $table = "estudiantes";
    public $timestamps = false;

    public function estudiantes_responsables()
    {
        return $this->hasMany(EstudiantesResponsables::class, 'estudiante_id', 'id')->with('responsables');
    }

    public function alergias()
    {
        return $this->hasMany(Alergias::class, 'estudiante_id', 'id');
    }
}
