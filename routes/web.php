<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'homeController@index')->name('inicio');


Route::get('/estudiante/listado', 'homeController@listadoEstudiantes')->name('listadoEstudiantes');
Route::get('/estudiante/nuevo', 'homeController@crear_estudiante')->name('crear_estudiante');
Route::post('/estudiante/nuevo/submt', 'homeController@crear_estudiante_submt')->name('crear_estudiante_submt');

Route::get('/estudiante/{id}/editar', 'homeController@editar_estudiante')->name('editar_estudiante');
Route::post('/estudiante/{id}/editar/submt', 'homeController@editar_estudiante_submt')->name('editar_estudiante_submt');

Route::get('/estudiante/{id}/eliminar', 'homeController@eliminar_estudiante')->name('eliminar_estudiante');

